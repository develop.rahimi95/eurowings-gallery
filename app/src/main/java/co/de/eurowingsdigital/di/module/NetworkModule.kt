package co.de.eurowingsdigital.di.module

import co.de.eurowingsdigital.BuildConfig
import co.de.eurowingsdigital.data.api.GalleryApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Sina Rahimi on 10/16/2020.
 */
@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {


    /**
     * Create a provider method binding for [GalleryApi].
     *
     * @return Instance of gallery service.
     * @see Provides
     */
    @Singleton
    @Provides
    fun provideApiGallery() : GalleryApi{
    val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val clientBuilder = OkHttpClient.Builder()
    val client = clientBuilder.addInterceptor(httpLoggingInterceptor).build()

    return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(GalleryApi::class.java)
    }

}