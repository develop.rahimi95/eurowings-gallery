package co.de.eurowingsdigital.di.module

import android.content.Context
import androidx.room.Room
import co.de.eurowingsdigital.data.db.GalleryDao
import co.de.eurowingsdigital.data.db.GalleryDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

/**
 * Created by Sina Rahimi on 10/18/2020.
 */
@InstallIn(ApplicationComponent::class)
@Module
object DataBaseModule {

    /**
     * Create a provider method binding for [GalleryDataBase].
     *
     * @return Instance of gallery database.
     * @see Provides
     */
    @Singleton
    @Provides
    fun provideDataBase(@ApplicationContext context: Context): GalleryDataBase {

        return Room.databaseBuilder(context, GalleryDataBase::class.java, "database").build()
    }

    /**
     * Create a provider method binding for [GalleryDao].
     *
     * @return Instance of Gallery data access object.
     * @see Provides
     */
    @Singleton
    @Provides
    fun provideGalleryDao(galleryDataBase: GalleryDataBase): GalleryDao {

        return galleryDataBase.galleryDao()
    }
}