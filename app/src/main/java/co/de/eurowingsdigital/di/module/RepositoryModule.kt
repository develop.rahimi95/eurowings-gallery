package co.de.eurowingsdigital.di.module

import co.de.eurowingsdigital.data.repository.GalleryRepository
import co.de.eurowingsdigital.data.repository.GalleryRepositoryImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

/**
 * Created by Sina Rahimi on 10/16/2020.
 */
@Module
@InstallIn(ApplicationComponent::class)
abstract class RepositoryModule {

    /**
     * Create a provider method binding for [GalleryRepository].
     *
     * @return Instance of gallery repository.
     * @see Binds
     */
    @Singleton
    @Binds
    abstract fun galleryRepository(galleryRepositoryImp: GalleryRepositoryImp): GalleryRepository
}