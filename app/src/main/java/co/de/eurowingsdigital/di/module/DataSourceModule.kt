package co.de.eurowingsdigital.di.module

import co.de.eurowingsdigital.data.datasource.gallery.GalleryLocalDataSource
import co.de.eurowingsdigital.data.datasource.gallery.GalleryLocalDataSourceImp
import co.de.eurowingsdigital.data.datasource.gallery.GalleryRemoteDataSource
import co.de.eurowingsdigital.data.datasource.gallery.GalleryRemoteDataSourceImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

/**
 * Created by Sina Rahimi on 12/4/2020.
 */
@Module
@InstallIn(ApplicationComponent::class)
abstract class DataSourceModule {

    @Binds
    @Singleton
    abstract fun galleryLocalDataSource(galleryLocalDataSourceImp: GalleryLocalDataSourceImp)
            : GalleryLocalDataSource

    @Binds
    @Singleton
    abstract fun galleryRemoteDataSource(galleryRemoteDataSourceImp: GalleryRemoteDataSourceImp)
            : GalleryRemoteDataSource
}