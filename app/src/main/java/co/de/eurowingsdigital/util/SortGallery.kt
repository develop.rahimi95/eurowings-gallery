package co.de.eurowingsdigital.util

/**
 * Created by Sina Rahimi on 10/13/2020.
 */
enum class SortGallery(val value: String) {
    VIRAL("viral"),
    TOP("top"),
    TIME("time")
}