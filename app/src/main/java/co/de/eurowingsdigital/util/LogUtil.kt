package co.de.eurowingsdigital.util

import android.util.Log
import co.de.eurowingsdigital.BuildConfig

/**
 * Created by Sina Rahimi on 10/25/2020.
 */
object LogUtil {

    fun d(key: String, message: String) {
        if (BuildConfig.DEBUG) {
            Log.d(key, message)
        }
    }

    fun e(key: String, message: String?) {
        if (BuildConfig.DEBUG) {
            message?.let { Log.e(key, it) }
        }
    }

    fun e(key: String, message: String?, throwable: Throwable) {
        if (BuildConfig.DEBUG) {
            Log.e(key, message, throwable)
        }
    }
}