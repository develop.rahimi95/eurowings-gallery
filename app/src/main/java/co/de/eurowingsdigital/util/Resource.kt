package co.de.eurowingsdigital.util

/**
 * Created by Sina Rahimi on 8/28/2020.
 */
data class Resource<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {
        fun <T> success(data: T): Resource<T> {
            return Resource(status = Status.SUCCESS, data = data, message = null)
        }

        fun <T> error(data: T?, message: String?): Resource<T> {
            return Resource(status = Status.ERROR, data = data, message = message)
        }
    }
}