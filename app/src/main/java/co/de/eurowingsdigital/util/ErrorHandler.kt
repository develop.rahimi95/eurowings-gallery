package co.de.eurowingsdigital.util

import co.de.eurowingsdigital.data.model.ApiError
import com.google.gson.GsonBuilder
import retrofit2.HttpException


/**
 * Created by Sina Rahimi on 10/17/2020.
 */
object ErrorHandler {

    @JvmStatic
    fun convertErrorBody(throwable: Throwable): ApiError {

        var apiError = ApiError()

        if (throwable is HttpException) {
            kotlin.runCatching {

                val gson = GsonBuilder().create()
                apiError =
                    gson.fromJson(throwable.response()?.errorBody()?.string(), ApiError::class.java)
            }.onSuccess {

                return apiError
            }.onFailure {
                throw throwable
            }
        }
        return apiError
    }
}