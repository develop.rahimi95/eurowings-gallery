package co.de.eurowingsdigital.util

/**
 * Created by Sina Rahimi on 10/13/2020.
 */
enum class WindowGallery(val value: String) {
    DAY("day"),
    WEEK("week"),
    MONTH("month"),
    YEAR("year"),
    ALL("all"),
}