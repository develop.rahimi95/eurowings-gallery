package co.de.eurowingsdigital.util

/**
 * Created by Sina Rahimi on 10/13/2020.
 */
enum class SectionGallery(val value: String) {
    HOT("hot"),
    TOP("top"),
    USER("user")
}