package co.de.eurowingsdigital.util

/**
 * Created by Sina Rahimi on 10/19/2020.
 */
enum class ErrorType (val value: String){
    UNKNOWN("unknown_error"),
    NETWORK("network_error")
}