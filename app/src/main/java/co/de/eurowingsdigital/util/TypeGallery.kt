package co.de.eurowingsdigital.util

/**
 * Created by Sina Rahimi on 10/17/2020.
 */
enum class TypeGallery(val value: String) {
    VIDEO("video/mp4"),
    IMAGE_PNG("image/png"),
    IMAGE_JPEG("image/jpeg"),
    GIF("image/gif")
}