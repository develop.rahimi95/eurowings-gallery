package co.de.eurowingsdigital.util

/**
 * Created by Sina Rahimi on 10/18/2020.
 */
enum class Status {
    SUCCESS, ERROR
}