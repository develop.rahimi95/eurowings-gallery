package co.de.eurowingsdigital.data.datasource.gallery

import co.de.eurowingsdigital.data.api.GalleryApi
import co.de.eurowingsdigital.data.model.Gallery
import kotlinx.coroutines.flow.Flow

/**
 * Created by Sina Rahimi on 12/3/2020.
 */
interface GalleryLocalDataSource {

    suspend fun insert(galleries: List<GalleryApi.Dto.Gallery>)

    fun galleries(): Flow<List<Gallery>>
}