package co.de.eurowingsdigital.data.repository

import co.de.eurowingsdigital.data.datasource.gallery.GalleryLocalDataSource
import co.de.eurowingsdigital.data.datasource.gallery.GalleryRemoteDataSource
import co.de.eurowingsdigital.data.model.Gallery
import co.de.eurowingsdigital.data.model.GalleryRequest
import co.de.eurowingsdigital.util.LogUtil
import co.de.eurowingsdigital.util.Resource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Created by Sina Rahimi on 10/14/2020.
 */
class GalleryRepositoryImp
@Inject constructor(
    private val galleryRemoteDataSource: GalleryRemoteDataSource,
    private val galleryLocalDataSource: GalleryLocalDataSource
) : GalleryRepository {

    companion object{
        const val TAG = "GalleryRepositoryImp"
    }

    override suspend fun refresh(galleryRequest: GalleryRequest): Resource<List<Gallery>> = try {

        //Doest need to have success here
        var list:List<Gallery> = listOf()
        val response = galleryRemoteDataSource.getGalleries(galleryRequest)

        galleryLocalDataSource.insert(response.data)

        Resource.success(list)
    } catch (throwable: Throwable) {
        
        LogUtil.e(TAG,throwable.message)

        Resource.error(null, "")
    }

    override fun observe(): Flow<List<Gallery>> =
        galleryLocalDataSource.galleries()
}