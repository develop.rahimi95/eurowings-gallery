package co.de.eurowingsdigital.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

/**
 * Created by Sina Rahimi on 10/18/2020.
 */

@Dao
interface GalleryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(galleries: List<GalleryEntity>)

    @Query("SELECT * FROM gallery_table")
    fun select(): Flow<List<GalleryEntity>>

}