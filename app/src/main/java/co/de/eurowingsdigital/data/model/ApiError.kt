package co.de.eurowingsdigital.data.model

/**
 * Created by Sina Rahimi on 10/17/2020.
 */

/**
 * @param success true or false
 * @param status error code of the request
 *  */
data class ApiError(val data: DataError? = null, val success: Boolean = false, val status: Int = 0)

/**
 * @param error error message
 * */
data class DataError(val error: String)
