package co.de.eurowingsdigital.data.mapper

import co.de.eurowingsdigital.data.db.GalleryEntity
import co.de.eurowingsdigital.data.model.Gallery

/**
 * Created by Sina Rahimi on 10/18/2020.
 */
fun GalleryEntity.map() = Gallery(
    id = id,
    upVote = upVote,
    downVote = downVote,
    title = title,
    description = description,
    score = score,
    link = link,
    animated = animated,
    datetime = datetime,
    type = type
)