package co.de.eurowingsdigital.data.datasource.gallery

import co.de.eurowingsdigital.data.api.GalleryApi
import co.de.eurowingsdigital.data.model.DataResponse
import co.de.eurowingsdigital.data.model.GalleryRequest

/**
 * Created by Sina Rahimi on 12/3/2020.
 */
interface GalleryRemoteDataSource {

    suspend fun getGalleries(galleryRequest: GalleryRequest): DataResponse<GalleryApi.Dto.Gallery>
}