package co.de.eurowingsdigital.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Sina Rahimi on 10/17/2020.
 */
@Parcelize
data class Gallery(
    val id: String,
    val upVote: Int?,
    val downVote: Int?,
    val title: String,
    val description: String?,
    val score: Int?,
    val link: String?,
    val animated: Boolean?,
    val datetime: Int?,
    val type: String?
):Parcelable