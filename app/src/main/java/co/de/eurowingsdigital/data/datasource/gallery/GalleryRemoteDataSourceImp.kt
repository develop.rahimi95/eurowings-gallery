package co.de.eurowingsdigital.data.datasource.gallery

import co.de.eurowingsdigital.data.api.GalleryApi
import co.de.eurowingsdigital.data.model.DataResponse
import co.de.eurowingsdigital.data.model.GalleryRequest
import javax.inject.Inject

/**
 * Created by Sina Rahimi on 12/3/2020.
 */
class GalleryRemoteDataSourceImp @Inject constructor(private val galleryApi: GalleryApi) :
    GalleryRemoteDataSource {

    //ClientId should be saved securely
    private val clientId = "Client-ID 404862e762056d0"

    override suspend fun getGalleries(galleryRequest: GalleryRequest)

            : DataResponse<GalleryApi.Dto.Gallery> {

        return galleryApi.getGalleries(
            galleryRequest.section,
            galleryRequest.sort,
            galleryRequest.window,
            galleryRequest.showViral,
            clientId
        )
    }
}