package co.de.eurowingsdigital.data.model

import co.de.eurowingsdigital.util.SectionGallery

/**
 * Created by Sina Rahimi on 12/4/2020.
 */
data class GalleryRequest(
    var section: String = "",
    var sort: String = SectionGallery.HOT.value,
    var window: String = "",
    var showViral: Boolean = false
)