package co.de.eurowingsdigital.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Sina Rahimi on 10/18/2020.
 */

@Entity(tableName = "gallery_table")
data class GalleryEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "up_vote") val upVote: Int?,
    @ColumnInfo(name = "down_vote") val downVote: Int?,
    @ColumnInfo(name = "score") val score: Int?,
    @ColumnInfo(name = "link") val link: String?,
    @ColumnInfo(name = "animated") val animated: Boolean?,
    @ColumnInfo(name = "datetime") val datetime: Int?,
    @ColumnInfo(name = "type") val type: String?
)

