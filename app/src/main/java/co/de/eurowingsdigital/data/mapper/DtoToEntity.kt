package co.de.eurowingsdigital.data.mapper

import co.de.eurowingsdigital.data.api.GalleryApi
import co.de.eurowingsdigital.data.db.GalleryEntity

/**
 * Created by Sina Rahimi on 10/18/2020.
 */

fun GalleryApi.Dto.Gallery.mapToEntity() = GalleryEntity(
    id = id,
    title = title,
    description = description,
    upVote = upVote,
    downVote = downVote,
    score = score,
    animated = images?.get(0)?.animated,
    link = images?.get(0)?.link,
    datetime = images?.get(0)?.datetime,
    type = images?.get(0)?.type
)