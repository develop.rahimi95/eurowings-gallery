package co.de.eurowingsdigital.data.repository

import co.de.eurowingsdigital.data.model.Gallery
import co.de.eurowingsdigital.data.model.GalleryRequest
import co.de.eurowingsdigital.util.Resource
import kotlinx.coroutines.flow.Flow

/**
 * Created by Sina Rahimi on 10/14/2020.
 */
interface GalleryRepository {

    suspend fun refresh(galleryRequest: GalleryRequest): Resource<List<Gallery>>

    fun observe(): Flow<List<Gallery>>
}