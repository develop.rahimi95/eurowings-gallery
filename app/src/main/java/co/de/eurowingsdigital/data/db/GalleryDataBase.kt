package co.de.eurowingsdigital.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import co.de.eurowingsdigital.BuildConfig

/**
 * Created by Sina Rahimi on 10/18/2020.
 */
@Database(
    entities = [GalleryEntity::class],
    exportSchema = false,
    version = BuildConfig.DATABASE_VERSION
)
abstract class GalleryDataBase : RoomDatabase()  {

    /**
     *
     * @return GalleryDao.
     */
    abstract fun galleryDao(): GalleryDao
}