package co.de.eurowingsdigital.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Sina Rahimi on 10/14/2020.
 */

data class DataResponse<T>(
    @SerializedName("success") val success: Boolean,
    @SerializedName("status") val status: Int,
    @SerializedName("data") val data: List<T>
)


