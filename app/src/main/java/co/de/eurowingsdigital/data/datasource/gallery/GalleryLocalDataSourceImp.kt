package co.de.eurowingsdigital.data.datasource.gallery

import co.de.eurowingsdigital.data.api.GalleryApi
import co.de.eurowingsdigital.data.db.GalleryDao
import co.de.eurowingsdigital.data.mapper.map
import co.de.eurowingsdigital.data.mapper.mapToEntity
import co.de.eurowingsdigital.data.model.Gallery
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * Created by Sina Rahimi on 12/3/2020.
 */
class GalleryLocalDataSourceImp @Inject constructor(private val galleryDao: GalleryDao) : GalleryLocalDataSource {
    override suspend fun insert(galleries: List<GalleryApi.Dto.Gallery>) {
        galleryDao.insert(galleries.map { it.mapToEntity() })
    }

    override fun galleries(): Flow<List<Gallery>> {
        return galleryDao.select().map {
            it.map {
                it.map()
            }
        }
    }


}