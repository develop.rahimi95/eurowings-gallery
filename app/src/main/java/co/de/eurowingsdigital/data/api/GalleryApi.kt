package co.de.eurowingsdigital.data.api

import co.de.eurowingsdigital.data.model.DataResponse
import com.google.gson.annotations.SerializedName
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Sina Rahimi on 10/13/2020.
 */
interface GalleryApi {

    @GET("gallery/{section}/{sort}/{window}")
    suspend fun getGalleries(
        @Path("section") section: String,
        @Path("sort") sort: String,
        @Path("window") window: String,
        @Query("showViral") showViral: Boolean,
        @Header("Authorization") clientId:String
    ):DataResponse<Dto.Gallery>

    sealed class Dto {

        data class Gallery(

            @SerializedName("id") var id: String,
            @SerializedName("title") var title: String,
            @SerializedName("score") var score: Int?,
            @SerializedName("ups") var upVote: Int?,
            @SerializedName("downs") var downVote: Int?,
            @SerializedName("description") var description: String?,
            @SerializedName("images")  var images: List<Image>?
        ):Dto()

        data class Image(

            @SerializedName("id") var id: String,
            @SerializedName("link") var link: String?,
            @SerializedName("animated") var animated: Boolean?,
            @SerializedName("datetime") var datetime: Int?,
            @SerializedName("type") var type: String?
        ):Dto()
    }
}