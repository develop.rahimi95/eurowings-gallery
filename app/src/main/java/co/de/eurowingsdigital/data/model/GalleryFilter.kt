package co.de.eurowingsdigital.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Sina Rahimi on 11/3/2020.
 */
@Parcelize
data class GalleryFilter(
    var section: String = "",
    var window: String = ""
) : Parcelable