package co.de.eurowingsdigital.feature.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import co.de.eurowingsdigital.R
import co.de.eurowingsdigital.data.model.GalleryRequest
import co.de.eurowingsdigital.feature.gallery.GalleryViewModel
import co.de.eurowingsdigital.util.SectionGallery
import co.de.eurowingsdigital.util.WindowGallery
import co.de.eurowingsdigital.util.WindowGallery.*
import co.de.eurowingsdigital.util.common.empty
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_filter_dialog.*

@AndroidEntryPoint
class FilterDialogFragment : BottomSheetDialogFragment(),
    View.OnClickListener {

    private val galleryViewModel: GalleryViewModel by viewModels()
    private var galleryRequest = GalleryRequest()

    companion object {

        @JvmStatic
        fun newInstance() =
            FilterDialogFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_filter_dialog, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.btn_cancel_filter -> {
                findNavController().popBackStack()
            }

            R.id.btn_apply_filter -> {

                galleryViewModel.refresh(galleryRequest)
                findNavController().navigate(
                    R.id.action_filterDialogFragment_to_galleryFragment
                )
            }

            R.id.checkBox_hot -> {
                if (checkBox_hot.isChecked) {
                    galleryRequest.section = SectionGallery.HOT.value
                    resetCheckBoxIsChecked(SectionGallery.HOT)

                } else {
                    empty()
                }
            }

            R.id.checkbox_top -> {
                if (checkbox_top.isChecked) {
                    galleryRequest.section = SectionGallery.TOP.value
                    resetCheckBoxIsChecked(SectionGallery.TOP)
                } else {
                    empty()
                }
            }

            R.id.checkbox_user -> {
                if (checkbox_user.isChecked) {
                    galleryRequest.section = SectionGallery.USER.value
                    resetCheckBoxIsChecked(SectionGallery.USER)
                } else {
                    empty()
                }
            }

            R.id.checkbox_day -> {
                if (checkbox_day.isChecked) {
                    galleryRequest.window = DAY.value
                    resetCheckBoxIsChecked(DAY)

                } else {
                    empty()
                }
            }

            R.id.checkbox_week -> {
                if (checkbox_week.isChecked) {
                    galleryRequest.window = WEEK.value
                    resetCheckBoxIsChecked(WEEK)

                }
            }

            R.id.checkbox_month -> {
                if (checkbox_month.isChecked) {
                    galleryRequest.window = MONTH.value
                    resetCheckBoxIsChecked(MONTH)

                } else {
                    empty()
                }
            }

            R.id.checkbox_year -> {
                if (checkbox_year.isChecked) {
                    galleryRequest.window = YEAR.value
                    resetCheckBoxIsChecked(YEAR)

                } else {
                    empty()
                }
            }
        }
    }

    //Set other checkBoxes.isChecked to false except the current one
    private fun resetCheckBoxIsChecked(sectionGallery: SectionGallery) {
        when (sectionGallery) {
            SectionGallery.USER -> {
                checkBox_hot.isChecked = false
                checkbox_top.isChecked = false
            }

            SectionGallery.HOT -> {
                checkbox_user.isChecked = false
                checkbox_top.isChecked = false
            }

            SectionGallery.TOP -> {
                checkbox_user.isChecked = false
                checkBox_hot.isChecked = false
            }
        }
    }

    //Set other checkBoxes.isChecked to false except the current one
    private fun resetCheckBoxIsChecked(windowGallery: WindowGallery) {
        when (windowGallery) {
            DAY -> {
                checkbox_week.isChecked = false
                checkbox_month.isChecked = false
                checkbox_year.isChecked = false
            }
            WEEK -> {
                checkbox_day.isChecked = false
                checkbox_month.isChecked = false
                checkbox_year.isChecked = false
            }
            MONTH -> {
                checkbox_day.isChecked = false
                checkbox_week.isChecked = false
                checkbox_year.isChecked = false
            }
            YEAR -> {
                checkbox_day.isChecked = false
                checkbox_week.isChecked = false
                checkbox_month.isChecked = false
            }
            else -> {}
        }
    }

    private fun initView() {
        btn_apply_filter.setOnClickListener(this)
        btn_cancel_filter.setOnClickListener(this)
        checkBox_hot.setOnClickListener(this)
        checkbox_top.setOnClickListener(this)
        checkbox_user.setOnClickListener(this)
        checkbox_day.setOnClickListener(this)
        checkbox_week.setOnClickListener(this)
        checkbox_month.setOnClickListener(this)
        checkbox_year.setOnClickListener(this)
    }
}