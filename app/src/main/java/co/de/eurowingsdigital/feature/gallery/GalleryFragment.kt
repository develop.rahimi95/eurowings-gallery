package co.de.eurowingsdigital.feature.gallery

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import co.de.eurowingsdigital.R
import co.de.eurowingsdigital.data.model.Gallery
import co.de.eurowingsdigital.util.Resource
import co.de.eurowingsdigital.util.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.gallery_content.view.*

@AndroidEntryPoint
class GalleryFragment : Fragment(R.layout.fragment_gallery) {

    private val galleryViewModel: GalleryViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        galleryViewModel.response.observe(viewLifecycleOwner, ::onResponse)
        galleryViewModel.galleries.observe(viewLifecycleOwner, ::onGalleries)
    }

    private fun onGalleries(galleries: List<Gallery>) {
        val listAdapter = GalleryAdapterListAdapter()
        listAdapter.submitList(galleries)

        val layoutManager = GridLayoutManager(activity, 3)
        view?.recycler_view_gallery?.layoutManager = layoutManager
        view?.recycler_view_gallery?.adapter = listAdapter
    }

    private fun onResponse(response: Resource<List<Gallery>>) {

        if (response.status != Status.SUCCESS) {
            Toast.makeText(requireContext(), response.message, Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun onRefresh() {
//        galleryViewModel.refresh()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {

        inflater.inflate(R.menu.menu_toolbar, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_filter -> {
                findNavController().navigate(R.id.action_galleryFragment_to_filterDialogFragment)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        @JvmStatic
        fun newInstance() = GalleryFragment()
    }
}