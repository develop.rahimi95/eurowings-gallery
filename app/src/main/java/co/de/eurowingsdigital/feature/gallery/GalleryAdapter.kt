package co.de.eurowingsdigital.feature.gallery

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import co.de.eurowingsdigital.R
import co.de.eurowingsdigital.data.model.Gallery
import co.de.eurowingsdigital.util.TypeGallery
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_gallery.view.*

/**
 * Created by Sina Rahimi on 10/14/2020.
 */
class GalleryAdapter(private val galleryList: ArrayList<Gallery>) :
    RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {

    private val requestOptionsOnline = RequestOptions().override(300, 300)
        .placeholder(ColorDrawable(Color.GRAY)).diskCacheStrategy(DiskCacheStrategy.ALL)
        .centerCrop()


    inner class ViewHolder(override val containerView: View?) :
        RecyclerView.ViewHolder(containerView!!),
        LayoutContainer {


        fun bind(gallery: Gallery, holder: ViewHolder) {
            val context = holder.itemView.context

            gallery.description?.let {
                holder.itemView.txt_description_gallery.text = gallery.description
            }

            val type = gallery.type
            val imageLink = gallery.link
            val dataTime = gallery.datetime
            if (type != null && imageLink != null && dataTime != null) {
                when (type) {

                    TypeGallery.GIF.value -> {

                        Glide.with(context)
                            .asGif()
                            .load(imageLink)
                            .signature(ObjectKey(dataTime))
                            .apply(requestOptionsOnline)
                            .thumbnail(0.5f)
                            .into(holder.itemView.image_gallery)
                    }

                    TypeGallery.VIDEO.value -> {

                        holder.itemView.image_gallery.setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                R.drawable.video_placeholder
                            )
                        )
                    }

                    else -> {
                        Glide.with(context)
                            .load(imageLink)
                            .signature(ObjectKey(dataTime))
                            .apply(requestOptionsOnline)
                            .thumbnail(0.5f)
                            .into(holder.itemView.image_gallery)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_gallery, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val galleryItem = galleryList[position]
        holder.bind(galleryItem, holder)
        holder.itemView.setOnClickListener { view ->
            val bundle = Bundle()
            bundle.putParcelable(GALLERY_BUNDLE, galleryItem)
            view.findNavController()
                .navigate(R.id.action_galleryFragment_to_detailGalleryFragment, bundle)
        }
    }

    override fun getItemCount(): Int {
        return galleryList.size
    }


    companion object {
        private const val GALLERY_BUNDLE = ""
//        fun getLuncherIntent():Intent{
//            Intent
//        }
    }
}