package co.de.eurowingsdigital.feature.detailgallery

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import co.de.eurowingsdigital.R
import co.de.eurowingsdigital.data.model.Gallery
import co.de.eurowingsdigital.util.TypeGallery
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import kotlinx.android.synthetic.main.fragment_detail_gallery.*

class DetailGalleryFragment : Fragment(R.layout.fragment_detail_gallery) {
    private lateinit var gallery: Gallery
    private val requestOptionsOnline = RequestOptions()
        .placeholder(ColorDrawable(Color.GRAY)).diskCacheStrategy(DiskCacheStrategy.ALL)
        .centerCrop()

    companion object {

        @JvmStatic
        fun newInstance() =
            DetailGalleryFragment()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            gallery = it.getParcelable("GALLERY_BUNDLE")!!
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleImage()
        setData()
    }

    private fun setData() {
        textView_image_title.text = gallery.title
        textView_image_description.text = gallery.description
        textView_image_upVotes.text = gallery.upVote.toString()
        textView_image_downVotes.text = gallery.downVote.toString()
        textView_image_score.text = gallery.score.toString()
    }

    private fun handleImage() {
        val dataTime = gallery.datetime

        if (gallery.type != null && gallery.link != null && dataTime != null) {
            when (gallery.type) {

                TypeGallery.GIF.value -> {

                    Glide.with(requireContext())
                        .asGif()
                        .load(gallery.link)
                        .signature(ObjectKey(dataTime))
                        .apply(requestOptionsOnline)
                        .into(image_big)
                }

                TypeGallery.VIDEO.value -> {

                    image_big.setImageDrawable(
                        ContextCompat.getDrawable(
                            requireContext(),
                            R.drawable.video_placeholder
                        )
                    )
                }

                else -> {
                    Glide.with(requireContext())
                        .load(gallery.link)
                        .signature(ObjectKey(dataTime))
                        .apply(requestOptionsOnline)
                        .into(image_big)
                }
            }
        }
    }

}