package co.de.eurowingsdigital.feature.gallery

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.de.eurowingsdigital.data.model.Gallery
import co.de.eurowingsdigital.data.model.GalleryRequest
import co.de.eurowingsdigital.data.repository.GalleryRepository
import co.de.eurowingsdigital.util.Resource
import co.de.eurowingsdigital.util.common.SingleLiveEvent
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Created by Sina Rahimi on 10/14/2020.
 */

class GalleryViewModel @ViewModelInject constructor(
    private val galleryRepository: GalleryRepository
) : ViewModel() {

    private val mutableGalleries = MutableLiveData<List<Gallery>>()
    val galleries: LiveData<List<Gallery>> = mutableGalleries
    private val mutableResponse = SingleLiveEvent<Resource<List<Gallery>>>()
    val response: LiveData<Resource<List<Gallery>>> = mutableResponse

    //TODO Replace LiveData with StateFlow

    init {
        viewModelScope.launch {
            galleryRepository.observe().collect {
                mutableGalleries.value = it
            }
        }

        refresh(GalleryRequest())
    }

    fun refresh(galleryRequest: GalleryRequest) = viewModelScope.launch {
            val response = galleryRepository.refresh(galleryRequest)
            mutableResponse.value = response
    }
}