package co.de.eurowingsdigital.feature.gallery

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import co.de.eurowingsdigital.R
import co.de.eurowingsdigital.data.model.Gallery
import co.de.eurowingsdigital.util.TypeGallery
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import kotlinx.android.synthetic.main.list_item_gallery.view.*

/**
 * Created by Sina Rahimi on 10/14/2020.
 */
class GalleryAdapterListAdapter :
    ListAdapter<Gallery, GalleryAdapterListAdapter.ViewHolder>(DiffCallback()) {

    private val requestOptionsOnline = RequestOptions().override(300, 300)
        .placeholder(ColorDrawable(Color.GRAY)).diskCacheStrategy(DiskCacheStrategy.ALL)
        .centerCrop()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(gallery: Gallery) {

            gallery.description?.let {
                itemView.txt_description_gallery.text = it
            }
            val context = itemView.context
            val type = gallery.type
            val imageLink = gallery.link

            val dataTime = gallery.datetime
            if (type != null && imageLink != null && dataTime != null) {
                when (type) {

                    TypeGallery.GIF.value -> {

                        Glide.with(context)
                            .asGif()
                            .load(imageLink)
                            .signature(ObjectKey(dataTime))
                            .apply(requestOptionsOnline)
                            .thumbnail(0.5f)
                            .into(itemView.image_gallery)
                    }

                    TypeGallery.VIDEO.value -> {

                        itemView.image_gallery.setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                R.drawable.video_placeholder
                            )
                        )
                    }

                    else -> {
                        Glide.with(context)
                            .load(imageLink)
                            .signature(ObjectKey(dataTime))
                            .apply(requestOptionsOnline)
                            .thumbnail(0.5f)
                            .into(itemView.image_gallery)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_gallery, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class DiffCallback : DiffUtil.ItemCallback<Gallery>() {
    override fun areItemsTheSame(oldItem: Gallery, newItem: Gallery): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Gallery, newItem: Gallery): Boolean {
        return oldItem == newItem
    }

}

