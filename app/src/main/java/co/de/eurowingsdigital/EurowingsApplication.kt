package co.de.eurowingsdigital

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Sina Rahimi on 10/14/2020.
 */

@HiltAndroidApp
class EurowingsApplication : Application()