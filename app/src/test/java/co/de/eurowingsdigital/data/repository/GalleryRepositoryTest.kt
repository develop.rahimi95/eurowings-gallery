package co.de.eurowingsdigital.data.repository

import android.util.Log
import co.de.eurowingsdigital.data.api.GalleryApi
import co.de.eurowingsdigital.data.db.GalleryDao
import co.de.eurowingsdigital.data.model.DataResponse
import co.de.eurowingsdigital.util.SectionGallery
import co.de.eurowingsdigital.util.SortGallery
import co.de.eurowingsdigital.util.WindowGallery
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test

/**
 * Created by Sina Rahimi on 11/9/2020.
 */
@ExperimentalCoroutinesApi
class GalleryRepositoryTest {

    //apiGallery is mocked
    private var apiGallery = mockk<GalleryApi>()

    //galleryDao is mocked
    private var galleryDao = mockk<GalleryDao>(relaxed = true)

    private lateinit var galleryRepository: GalleryRepositoryImp

    //Default parameters
    private val section = SectionGallery.HOT.value
    private val window = WindowGallery.WEEK.value
    private val sort = SortGallery.TOP.value
    private val showViral: Boolean = true
    private val clientId = "123456"
    private val errorMsg = "error"

    @Before
    fun setUp() {


        mockkStatic(Log::class)
        every { Log.v(any(), any()) } returns 0
        every { Log.d(any(), any()) } returns 0
        every { Log.i(any(), any()) } returns 0
        every { Log.e(any(), any()) } returns 0
    }

    @After
    fun tearDown() {
    }

    //`should saveGallery when getGallery response is successful`
    @Test
    fun testGallery() = runBlockingTest {

        galleryRepository = GalleryRepositoryImp(apiGallery, galleryDao)

        //Give
        coEvery {
            apiGallery.getGalleries(any(), any(), any(), any(), any())
        } returns successResponse()
        coEvery {
            galleryDao.getGallery()
        } returns arrayListOf()

        //when
        galleryRepository.refresh(section, sort, window, showViral)

        //Then
        coVerify(atLeast = 1) { galleryDao.insert(arrayListOf()) }
    }

    //generate fake data
    private fun successResponse(): DataResponse<GalleryApi.Dto.Gallery> {
        return DataResponse(true, 200, arrayListOf())
    }
}