package co.de.eurowingsdigital.util

import java.io.InputStreamReader

/**
 * Created by Sina Rahimi on 11/11/2020.
 */
class MockResponseFileReader(path: String) {

    val content: String
    init {
        val reader = InputStreamReader(this.javaClass.classLoader!!.getResourceAsStream(path))
        content = reader.readText()
        reader.close()
    }


}