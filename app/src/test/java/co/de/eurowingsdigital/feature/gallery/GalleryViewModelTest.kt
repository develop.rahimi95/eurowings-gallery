package co.de.eurowingsdigital.feature.gallery

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.de.eurowingsdigital.data.model.Gallery
import co.de.eurowingsdigital.data.model.GalleryFilter
import co.de.eurowingsdigital.data.repository.GalleryRepository
import co.de.eurowingsdigital.util.Resource
import co.de.eurowingsdigital.util.SectionGallery
import co.de.eurowingsdigital.util.SortGallery
import co.de.eurowingsdigital.util.WindowGallery
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by Sina Rahimi on 10/24/2020.
 */
@ExperimentalCoroutinesApi
class GalleryViewModelTest {

    private val galleryRepository = mockk<GalleryRepository>()

    private lateinit var galleryViewModel: GalleryViewModel

    private val galleryFilter = GalleryFilter(
        SectionGallery.HOT.value,
        WindowGallery.WEEK.value
    )
    private val sort = SortGallery.TOP.value
    private val showViral: Boolean = true
    private val errorMsg = "error"

    private val galleryList = ArrayList<Gallery>()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @ObsoleteCoroutinesApi
    @Before
    fun before() {

        mockkStatic(Log::class)
        every { Log.v(any(), any()) } returns 0
        every { Log.d(any(), any()) } returns 0
        every { Log.i(any(), any()) } returns 0
        every { Log.e(any(), any()) } returns 0

        Dispatchers.setMain(mainThreadSurrogate)
    }

    @Test
    fun `should success when get gallery return error`() = runBlocking {
        galleryViewModel = GalleryViewModel(galleryRepository)

        //Given
        coEvery {
            galleryRepository.refresh(any(), any(), any(), any())
        } returns Resource.error(null, errorMsg)

        galleryViewModel.galleries.observeForever {}

        //When
        galleryViewModel.setGalleryFilter(
            galleryFilter,
            sort,
            showViral
        )

        //Then
        assert(galleryViewModel.response.value == errorMsg)
    }

    @Test
    fun `should success when get gallery return success`() = runBlockingTest {
        galleryViewModel = GalleryViewModel(galleryRepository)

        //Given
        coEvery {
            galleryRepository.refresh(any(), any(), any(), any())
        } returns Resource.success(galleryList)

        galleryViewModel.galleries.observeForever {}

        //When
        galleryViewModel.setGalleryFilter(
            galleryFilter,
            sort,
            showViral
        )

        //Then
        assert(galleryViewModel.galleries.value == galleryList)
    }

    @ObsoleteCoroutinesApi
    @After
    fun after() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }
}