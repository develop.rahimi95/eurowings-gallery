# Gallery

###  Behind the app: ###
The goal was to show gallery from the server and cache them.

### How to run the app: ###
This app uses [The imgur public API](https://apidocs.imgur.com/).
with client_id, 

### Cache Image### 
for Caching images I used Glide to handle the image caching and Room for cache
the url of the image.

### Dependency Injection### 
For dependency injection i used Hilt,
It is built on top of the popular DI library Dagger with less boilerplate.

For handling threads I used Coroutine because it's compatible with android Jetpack libraries
like LiveData and Room.

For handling fragments I used Navigation component because it simplifies navigation between fragments

### Used libraries: ###
- [Retrofit2]
- [Navigation]
- [Hilt]
- [Kotlin Coroutines]
- [Glide]
- [Android_architecture_components]
- [Room Persistence Library]
